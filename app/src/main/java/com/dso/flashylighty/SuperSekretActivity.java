package com.dso.flashylighty;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;
import java.security.MessageDigest;

public class SuperSekretActivity extends AppCompatActivity {
    EditText username,password;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_sekret);
        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
        btnLogin=findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Objects.equals(username.getText().toString(), "xYzKiRiToxYz") && Objects.equals(genSha256(password.getText().toString()),"1f413f06cb30df064361e85d11c5da61e06db232e57f5b44cd3d33ab4a92e08e"))
                {
                    showAlert("Congrats!", generateFlag(password.getText().toString()));
                }else {
                    Toast.makeText(SuperSekretActivity.this,"Authentication Failed Successfully ;)",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showAlert(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).show();
    }

    private String generateFlag(String input) {
        char[] flag = input.toCharArray();
        try{
            flag[0] ^= 0x20;
            flag[1] ^= 0x56;
            flag[2] ^= 0x56;
            flag[4] ^= 0x58;
            flag[6] ^= 0x20;
            flag[9] ^= 0x20;
            flag[12] ^= 0x56;
            flag[14] ^= 0x46;
            flag[16] ^= 0x58;
            flag[17] ^= 0x46;
            flag[20] ^= 0x21;
            flag[22] ^= 0x20;
        } catch(Exception e) {
            Log.w("generateFlag", "Oh no.");
        }
        return new String(flag);
    }

    public static String genSha256(String input) {
        try {
            MessageDigest digest = null;
            digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            return bin2hex(digest.digest(input.getBytes()));
        } catch (Exception ignored) {
            return null;
        }
    }

    private static String bin2hex(byte[] data) {
        StringBuilder hex = new StringBuilder(data.length * 2);
        for (byte b : data)
            hex.append(String.format("%02x", b & 0xFF));
        return hex.toString();
    }
}
